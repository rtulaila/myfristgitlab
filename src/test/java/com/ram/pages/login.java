package com.ram.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class login {

	@Test
	public void test1() {
	 
		System.setProperty("webdriver.chrome.driver", "C:\\Drivers\\chrome\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("http://automationpractice.com/index.php?controller=authentication");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.name("email")).sendKeys("ramesh@web.com");
		driver.findElement(By.name("passwd")).sendKeys("123456");
		
		driver.findElement(By.name("SubmitLogin")).click();
		
		System.out.println("Test Case Passed");
		System.out.println("User is able to Login to Automation Practice Website");
		System.out.println("Maven Build is Running");
		System.out.println("TestNG is an automation testing framework ");
			
		
		driver.quit();

	}

}
